%%%%%%%%%%%%%%%%%%%%%%%%%%%% FOR JHEPcls 3.1.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% vim: set tw=120:

%\documentclass[]{JHEP3} % 10pt is ignored!

\documentclass[11pt,usletter]{article}
\usepackage{jheppub}
\usepackage[utf8]{inputenc}
\usepackage{epsfig,multicol}
\usepackage[sort&compress,numbers]{natbib}
\usepackage{graphicx}
%= definitions =================================================================
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\nn}{\nonumber}
\newcommand{\brem}{bremsstrahlung\ }
%===============================================================================


\title{A Projective Phase Space Generator for MCFM}

\author[a]{Walter T.~Giele}
\affiliation[a]{Theory Group, Fermilab, Batavia, USA}
\emailAdd{giele@fnal.gov}

\abstract{
This is a note on the implementation of FBPS into MCFM. In addition we add
a variant of the Czakon subtraction scheme (STRIPPER) which is tuned to FBPS.}

\keywords{QCD, Hadron Colliders, LHC}
\preprint{preprint FERMILAB-PUB-xx-yyy-T}
\arxivnumber{xxxx.yyyy}
\notoc
\begin{document}
\maketitle

\newpage

\section{Introduction}

A Forward Branching Phase Space (FBPS) generator for integrating bremsstrahlung Matrix Elements (ME) for calculating
higer order corrections to scattering amplitudes uses the Born event (the lowest order prediction of the observed final state)
to generate a branching analogous to shower monte carlo's. That is, given a Born event the jet/clustering algorithm will restore
the Born event. This enables the option to calculate $K$-factors for the Born events.
To branch a parton it needs a mass like is done in shower MC's. That is, in 
a shower MC the massless
momentum can be given a mass and decayed $p=(p_x,p_y,p_z,E)\equiv (0,\vec p_T,\eta)\rightarrow (m,\vec p_T,\eta)$ leaving the transverse
momentum, $p_T$ and rapidity, $\eta$,identical and subsequently decayed. 
For a fixed order phase space integrator we have to make this more exact
$p=(p_x,p_y,p_z,E)\equiv (\vec p_T,\vec p_L)\rightarrow (\vec p_T,(1-\alpha)\vec p_L)$. That is by rescaling the longitudinal component
of the 4-vector we can generate the mass while leaving the observable tranverse momentum and rapidity invariant. 
Momentum conservation can be maintained by absorbing the vector $(\vec 0,\alpha\vec p_L)$ into the unobservable initial momenta 
(i.e. adjust the parton fractions). By doing so we have exact phase space integration, while 4-vector clustering followed by the rescaling
of the longitudinal momentum of the jet axis to make it massless will give us back the born momentum of the jet.

As derived in [1806.09678], 
given the Born phase space point $ \hat p_a+\hat p_b=p_{ab}=\hat p_J + \hat P$ where $\hat p_a^2=\hat p_b^2=\hat p_J^2=0$ 
and $P$ is a set of momenta of the remainder of the event, we have the phase space generator which integrates over all possible 
branchings of $\hat p_J\rightarrow p_1+p_2$
\beq
d\Phi(p_{ab};p_1,p_2,P)=d\Phi(\hat p_{ab};\hat p_J,P)\times d\Phi(\hat p_J;p_1,p_2)\times\delta(\hat p_{ab}-p_{ab}-(1-\beta_+)\hat p_J^L)
\eeq
with
\beq
 d\Phi(\hat p_J;p_1,p_2)=\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]
\times J(\hat p_J,p_1)\ \delta(\hat p_J^T-p_1^T-p_2^T)\,\delta(\beta_+ p_J^L-p_1^L-p_2^L)
\eeq
where
\beqa
J(\hat p_j,p_1)&=&\left|\frac{2}{1-\beta_-/\beta_+}\right|\times\sqrt{\frac{(\hat p_J^T)^2}{(\hat p_J^L)^2}}
=\left|\frac{2}{1-\beta_-/\beta_+}\right|\times\sqrt{1-\frac{\hat p_J^2}{(\hat p_J^L)^2}}
 \nonumber \\
\beta_{\pm}&=&\frac{(\hat p_J^L\cdot p_1^L)\pm\sqrt{(\hat p_J^T)^4+2(\hat p_J^L)^2(\hat p_J^T\cdot p_1^T)+(\hat p_J^L\cdot p_1^L)^2}}
{(\hat p_J^L)^2}
\eeqa
or after integrating out $p_2$ over the delta-functions
\beq
d\Phi(p_{ab};p_1,p_2,P)=d\Phi(\hat p_{ab};\hat p_J,P)\times\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]\times J(\hat p_J,p_1)
\eeq
with $p_2^T=\hat p_J^T-p_1^T$, $p_2^L=\beta_+\hat p_J^L-p_1^L$, $p_{ab}=\hat p_{ab}-(1-\beta_+)\hat p_J^L$.
That is adding a branching is adding a factorized one-particle integration to the Born phase space point. 
In other words for an observable ${\cal O}$ depending on the  

\bibliographystyle{JHEP}
\bibliography{mcfm_fbps.bib}
\end{document}

%%  LocalWords:  hadronic
