%%%%%%%%%%%%%%%%%%%%%%%%%%%% FOR JHEPcls 3.1.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% vim: set tw=120:

%\documentclass[]{JHEP3} % 10pt is ignored!

\documentclass[11pt,usletter]{article}
\usepackage{jheppub}
\usepackage[utf8]{inputenc}
\usepackage{epsfig,multicol}
\usepackage[sort&compress,numbers]{natbib}
\usepackage{graphicx}
%= definitions =================================================================
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}
\newcommand{\nn}{\nonumber}
\newcommand{\brem}{bremsstrahlung\ }
%===============================================================================


\title{A Projective Phase Space Generator for MCFM}

\author[a]{Walter T.~Giele}
\affiliation[a]{Theory Group, Fermilab, Batavia, USA}
\emailAdd{giele@fnal.gov}

\abstract{
This is a note on the implementation of FBPS into MCFM. In addition we add
a local subtraction scheme (akin to Czakon/Melnikov subtraction scheme). This scheme matches well with
FBPS but will work also without FBPS integration. In addition we will explore a method where we use
explicit antenna functions with laurent expansion of the singularies using PS paramatrized by invariants.
This should be a local subtraction as well.}

\keywords{QCD, Hadron Colliders, LHC}
\preprint{preprint FERMILAB-PUB-xx-yyy-T}
\arxivnumber{xxxx.yyyy}
\notoc
\begin{document}
\maketitle

\newpage

\section{A very brief introduction to FBPS}

A Forward Branching Phase Space (FBPS) generator for integrating bremsstrahlung Matrix Elements (ME) for calculating
higher order corrections to scattering amplitudes uses the Born event (defined as the lowest order prediction of the observed final state)
to generate the phase space through branching analogous to shower monte carlo's. 
That is, given a Born event the jet/clustering algorithm will restore
the Born event. Integrating subsequently over the Born phase space will produce the entire bremsstrahlung phase space.
This enables the option to calculate $K$-factors for the Born events.
See Refs.~\cite{Figy:2018imt,Chen:2019flj} for details.

To branch a parton it needs a mass like is done in shower MC's. The massless
momentum can be given a mass and decayed $p=(p_x,p_y,p_z,E)\equiv (0,\vec p_T,\eta)\rightarrow (m,\vec p_T,\eta)$ leaving the transverse
momentum, $p_T$ and rapidity, $\eta$, identical. Subsequently we can decay/branch the parton. If the resulting momenta are clustered,
$p_J=(M_{12},\vec p_{12}^T,\eta_{12})$ we can recover the original Born jet by zeroing the mass $M_{12}$, i.e. 
$p=(0,\vec p_{12}^T,\eta_{12})\rightarrow p_J^{\mbox{\tiny BORN}}=(p_x,p_y,p_z,E)$. The FBPS integrates over the jet mass 

For a fixed order phase space integrator we have to make this more exact. Define $p=(p_x,p_y,p_z,E)\equiv (\vec p_T,\vec p_L)$. Then
we can rescale the longitudinal component of the momentum, $\vec p_L$ without changing the transverse momentum and rapidity. We can
absorb the rescaling factor into the initial state (i.e. changing the parton fractions). 
Given the Born phase space point $\hat p_a+\hat p_b=\hat p_{ab}=\hat p_J + \hat P$ where $\hat p_a^2=\hat p_b^2=\hat p_J^2=0$ 
and $P$ is a set of momenta of the remainder of the event, we can apply the rescaling 
$\hat p_{ab}-\alpha\hat p_L=(\hat p_J+(1-\alpha)\hat p_L)+\hat P$.
Using the rescaling of the longitudinal component of a jet we can construct a phase space generator which integrates over all possible 
branchings of $\hat p_J\rightarrow p_1+p_2$
\beq
d\Phi(p_{ab};p_1,p_2,P)=d\Phi(\hat p_{ab};\hat p_J,P)\times d\Phi(\hat p_J;p_1,p_2)\times\delta(\hat p_{ab}-p_{ab}-(1-\beta_+)\hat p_J^L)
\eeq
with
\beq
 d\Phi(\hat p_J;p_1,p_2)=\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]
\times J(\hat p_J,p_1)\ \delta(\hat p_J^T-p_1^T-p_2^T)\,\delta(\beta_+ p_J^L-p_1^L-p_2^L)
\eeq
where
\beqa
J(\hat p_j,p_1)&=&\left|\frac{2}{1-\beta_-/\beta_+}\right|\times\sqrt{\frac{(\hat p_J^T)^2}{(\hat p_J^L)^2}}
=\left|\frac{2}{1-\beta_-/\beta_+}\right|\times\sqrt{1-\frac{\hat p_J^2}{(\hat p_J^L)^2}}
 \nonumber \\
\beta_{\pm}&=&\frac{(\hat p_J^L\cdot p_1^L)\pm\sqrt{(\hat p_J^T)^4+2(\hat p_J^L)^2(\hat p_J^T\cdot p_1^T)+(\hat p_J^L\cdot p_1^L)^2}}
{(\hat p_J^L)^2}
\eeqa
or after integrating out $p_2$ over the delta-functions
\beq
d\Phi(p_{ab};p_1,p_2,P)=d\Phi(\hat p_{ab};\hat p_J,P)\times\left[\frac{d p_1}{(2\pi)^3}\delta(p_1^2)\right]\times J(\hat p_J,p_1)
\eeq
with $p_2^T=\hat p_J^T-p_1^T$, $p_2^L=\beta_+\hat p_J^L-p_1^L$, $p_{ab}=\hat p_{ab}-(1-\beta_+)\hat p_J^L\rightarrow x_{a/b}\sqrt{S}=E_{ab}\pm p_Z^{ab}$.
As we see, the brancher is a factorized one-particle integration with a fixed Born. Any observable is defined using the transverse momenta
 and rapidities of the Born momenta 
and the brancher integration will contribute to the $K$-factor of this event. The brancher integration will have some jet constraints as
the total number of jets has to be equal th the number of Born jets. One option is to have a jet size and make the required number of jets exclusive,
i.e. equal to the number of Born jets. Alternatively, in an inclusive jet definition you can keep custering until the number of jets is equal to the Born number of jets.
As long as the clustering follows the usual procedures (1) the parton pair with the smallest distance is clustered and 
(2) the clustering is performed by adding the two 4-vectors, the reconstructed jets will hvae the identical $p_T$ and rapidity of the Born jets.

\section {Processes}

We will consider processes with increasing leve of complexity for both implementing FBPS and a local subtraction scheme. 
For the local subtraction schemes see Refs.~\cite{Czakon:2010td,Caola:2017dug}. In the processes $V$ stands for a massive EW
particle or a group of EW massive particles which can decay into massless particles.

\subsection{$PP\rightarrow V$}

For this process we will primarily focus on the subtraction scheme and we will closely follow ref.~\cite{Caola:2017dug}. 
In addition we will also explore the possibility of using antenna functions combined with Laurent expansions.
The FBPS in this case is a special case as there are no Born jets. However we can still factor out the Born
kinematic variables of the vector boson (its rapidity and mass). This would mean one calculates $d\sigma/d\eta_V d M_V$
and its $K$-factor (not taking the kinematics of the vector boson decay into account). This follows the spirit of FBPS
but in a rather limited manner.

\subsubsection{NLO}
In this section we look at the partonic $q\bar q\rightarrow V$ cross section where $V$ is a collection of massive EW particles.
We start with the method developed in ref.~\cite{Caola:2017dug} and follow this closely in notation and approach. The derivations are in 
section 3 of the aforementioned paper. We have the folowing notation:
\beqa
d\sigma(Q)&=&\int d x_1 d x_2 f_q(x_1)f_{\bar q} (x_2) d\hat\sigma_{q\bar q}(x_1,x_2,Q) \nonumber \\
d\hat\sigma_{q\bar q}&=&d\hat\sigma_{q\bar q}^{\mbox{\tiny LO}}+d\hat\sigma_{q\bar q}^{\mbox{\tiny NLO}}+d\hat\sigma_{q\bar q}^{\mbox{\tiny NNLO}}+\cdots\nonumber \\
d\hat\sigma_{q\bar q}^{\mbox{\tiny NLO}}(Q)&=&d\hat\sigma_{q\bar q}^{\mbox\tiny R}(Q)+d\hat\sigma_{q\bar q}^{\mbox\tiny V}(Q)
+\frac{\alpha_s(\mu)}{2\pi\varepsilon}\left(\hat P_{qq}^{(0)}\otimes d\hat\sigma^{\mbox{\tiny LO}}+d\hat\sigma^{\mbox{\tiny LO}}\otimes \hat P_{qq}^{(0)}\right)\nonumber \\
2S d\hat\sigma_{q\bar q}^{\mbox{\tiny LO}}(Q)&\equiv&\left\langle F_{\mbox{\tiny LM}}(p_1,p_2)\right\rangle
=\int d\mbox{Lips}_V \left|{\cal M}^{(0)}(p_1,p_2,Q)\right|^2 {\cal O}(p_1,p_2,Q)\nonumber \\
2S d\hat\sigma_{q\bar q}^{\mbox\tiny R}(Q)&\equiv&\left\langle F_{\mbox{\tiny LM}}(p_1,p_2,p_4)\right\rangle=\int\frac{d^{d-1} p_4}{(2\pi)^d 2E_4}\theta(E_{\mbox{\tiny max}}-E_4) 
F_{\mbox{\tiny LM}}(p_1,p_2,p_4)\nonumber \\
F_{\mbox{\tiny LM}}(p_1,p_2,p_4)&\equiv&\int d\mbox{Lips}_V\left|{\cal M}^{(0)}(p_1,p_2,p_4,Q)\right|^2{\cal O}(p_1,p_2,p_4,Q)\nonumber\\
2S d\hat\sigma_{q\bar q}^{\mbox\tiny V}(Q)&\equiv&\int d\mbox{Lips}_V2\mbox{Re}\left({\cal M}^{(0)}(p_1,p_2,Q)\times {{\cal M}^{(1)}(p_1,p_2,Q)}^\dagger\right) {\cal O}(p_1,p_2,Q)
\nonumber\\
&=&-2\left[\alpha_S\right]\cos(\varepsilon\pi)\left(\frac{C_F}{\varepsilon^2}+\frac{3}{2}\frac{C_F}{\varepsilon}\right)S^{-\varepsilon}
\left\langle F_{\mbox{\tiny LM}}(p_1,p_2)\right\rangle+\left\langle F_{\mbox{\tiny LM}}^{\mbox{\tiny fin}}(p_1,p_2)\right\rangle
\eeqa
where 
\beqa
%F_{\mbox{\tiny LM}}^{\mbox{\tiny fin}}(p_1,p_2)&=& \nonumber \\
\hat P_{qq}^{(0)}(z)&=&C_F\left[2{\cal D}_0(z)-(1+z)+\frac{3}{2}\delta(1-z)\right] \nonumber \\
{\cal P}_{qq,R}^{(\varepsilon)}(z)&=&C_F\Big[2(1+z)\ln(1-z)-(1-z)-4{\cal D}_1(z)\Big] \nonumber \\
{\cal D}_n(z)&=&\frac{\ln^n(1-z)}{[1-z]_+} \nonumber \\
\left[\alpha_S\right]&=&\frac{\alpha_s(\mu)}{2\pi}\frac{\mu^{2\varepsilon} e^{\varepsilon\gamma_E}}{\Gamma(1-\varepsilon)}
\eeqa
and $E_{\mbox{\tiny max}}$ is a arbitrary large parameter large enough to accomodate all possible kinematic configurations, $d\mbox{Lips}_V$ is the Lorentz invariant
phase space for colorless particles including the momentum conserving kronecter delta function, ${\cal O}$ is an infrared safe observable, $z=1-E_4/E_1$, $C_F=4/3$,
$F_{\mbox{\tiny LM}}^{\mbox{\tiny fin}}(p_1,p_2)$ is the part of the virtual corrections which is free of singularities and $\mu$-dependence and defined in the Catani
representation~\cite{Catani:1998bh}. It will be process dependent. 

The NLO cross section is now given by
\beqa
2S d\hat\sigma^{\mbox{\tiny NLO}}&=&\left\langle \hat O_{\mbox{\tiny NLO}} F_{\mbox{\tiny LM}}(p_1,p_2,p_4)\right\rangle \nonumber \\
&+&\left\langle F_{\mbox{\tiny LV}}^{\mbox{\tiny fin}}(p_1,p_2)+\frac{\alpha_s(\mu)}{2\pi}\left[\frac{3}{2}\pi^2 C_FF_{\mbox{\tiny LM}}(p_1,p_2)\right]\right\rangle \nonumber \\
&+&\frac{\alpha_s(\mu)}{2\pi}\int_0^1d z\left[\ln\left(\frac{S}{\mu^2}\right)\hat P_{qq}^{(0)}(z)-{\cal P}_{qq,R}^{(\varepsilon)}(z)\right]
\left\langle\frac{F_{\mbox{\tiny LM}}(z\cdot p_1,p_2)}{z}+\frac{F_{\mbox{\tiny LM}}(p_1,z\cdot p_2)}{z}\right\rangle \nonumber \\
\eeqa
where
\beqa
\hat O_{\mbox{\tiny NLO}}&=&(I-C_{41}-C_{42})(I-S_4) \nonumber \\
I\,F&=&F;\ S_iF=\lim_{E_i\rightarrow 0}F;\ C_{ij}F=\lim_{\rho_{ij}\rightarrow 0}F;\ \rho_{ij}=1-n_i\cdot n_j=1-\cos\phi_{ij} \nonumber \\
S_4 F_{\mbox{\tiny LM}}(p_1,p_2,p_4)&=&\frac{C_F g_{s,b}^2}{E_4^2}\frac{2\rho_{12}}{\rho_{14}\rho_{42}} F_{\mbox{\tiny LM}}(p_1,p_2) \nonumber \\
C_{41} F_{\mbox{\tiny LM}}(p_1,p_2,p_4)&=&\frac{g_{s,b}^2}{E_4^2\rho_{41}}(1-z) P_{qq(z)}\frac{F_{\mbox{\tiny LM}(p_1,p_2)}}{z};\ 
P_{qq}(z)=C_F\left[\frac{1+z^2}{1-z}-\varepsilon(1-z)\right]
\eeqa
and $g_{s,b}$ is the bare QCD coupling, $n_i$ is the unit vector in the direction of the momentum vector. 
The amplitude functions $F_{\mbox{\tiny LM}}(p_1,p_2)$, $F_{\mbox{\tiny LV}}^{\mbox{\tiny fin}}(p_1,p_2)$ and $F_{\mbox{\tiny LM}}(p_1,p_2,p_4)$
should be provided by MCFM.

Before embarking on NNLO it is worthwhile to consider antenna functions combined with Laurent expansion of its diverencies. This might have certain
advantages over the seperation of collinear and soft limits. The advantage of antenna functions
is that is combines soft and collinear in a single function which greatly simplifies the method as it avoids the nested overlapping limits.
Secondly everything is formulated without a reference frame, i.e it is Lorentz invariant.
However the drawback is that this is an antenna-dipole approach. This means it is related to a $3\rightarrow 2$ clustering and would not work well
with traditional jet algorithms, which use $2\rightarrow 1$ clustering. This would mean the FBPS approach would need a significant change also. 
This means that for now it is best to avoid this approach and stick with the Melnikov method.

Finally we consider FBPS in this case. As no final state jets are available the case is somewhat pathological. Yet, implementing it might be elumination.
The LO $q(p_1)q(p_2)\rightarrow V(Q)$ has just two observable independent kinematic variables: the rapidity $\eta_V$ and the vector boson mass $Q^2$. (
If $V$ is a collection of massive EW particles we can fix the rapidity and mass of each of those particles.) Within the context of FBPS we calculate
\beqa
\frac{d\sigma_{qq}}{d\eta_V d Q}&=&\frac{d\sigma_{qq}^{\mbox{\tiny LO}}}{d\eta_V d Q}+\frac{d\sigma_{qq}^{\mbox{\tiny NLO}}}{d\eta_V d Q}
                                   +\frac{d\sigma_{qq}^{\mbox{\tiny NNLO}}}{d\eta_V d Q}+\cdots \nonumber \\
&=&(1+K_{qq}^{\mbox{\tiny NLO}}+K_{qq}^{\mbox{\tiny NNLO}}+\cdots)\frac{d\sigma_{qq}^{\mbox{\tiny LO}}}{d\eta_V d Q}
\eeqa
This fixes the LO contribution to a single phase space event, and at higher orders we integrate over te $\vec p_T$ of the vector boson(s) while leaving $\eta_V$ and 
$Q^2$ invariant. The vectorboson $p_T$ distribution is inclusive.
To make connection to the experiment we need to shower the LO event to make the vectorboson $p_T$ exclusive preferably using a matched shower.

Specifically for this process, $p_1+p_2\rightarrow V$ at we have
\beqa
2S\frac{d\hat\sigma_{qq}^{\mbox{\tiny LO }}(Q)}{d\eta_V d M_V}&=&\frac{1}{4\pi^3}
\delta\left(x_1-\frac{M_V}{\sqrt{S}}e^{\eta_V}\right)\delta\left(x_2-\frac{M_V}{\sqrt{S}}e^{-\eta_V}\right)
\left|{\cal M}^{(0)}(x_1,x_2,Q)\right|^2\nonumber \\
Q_\mu&=&\left(\vec 0,M_V\sinh\eta_V,M_V\cosh\eta_V\right)
\eeqa

For higher order corrections we have additional partons we need to integrate out while keeping $M_V$ and $\eta_V$ fixed. This is particularly simply 
for this simple case and the n-parton treelevel contribution is given by
\beqa
2S\frac{d\hat\sigma_{qq}^{\mbox{\tiny R}}(Q)}{d\eta_V d M_V}&=&\frac{4}{(16\pi^3)^{n+1}}\left(\prod_{i=1}^nd p_T^{(i)}d\eta_i d\phi_i\times p_T^{(i)}\right)
\times\left|{\cal M}^{(0)}(x_1,x_2,Q,p_1,\ldots,p_n)\right|^2\nonumber \\
&&\times\delta\left(x_1-\frac{1}{\sqrt{S}}\left(\alpha_T e^{\eta_V}+\sum_{i=1}^n p_T^{(i)} e^{\eta_i}\right)\right)
\delta\left(x_2-\frac{1}{\sqrt{S}}\left(\alpha_T e^{-\eta_V}+\sum_{i=1}^n p_T^{(i)} e^{-\eta_i}\right)\right) 
\nonumber \\
\eeqa
where
\beqa
Q_\mu&=&\left(\vec q_T,\alpha_T\sinh\eta_V,\alpha_T\cosh\eta_V\right);\ \alpha_T=\sqrt{q_T^2+M_V^2}\nonumber \\
\vec q_T&=&-\sum_{i=1}^np_T^{(i)} \nonumber \\
p_\mu^{(i)}&=&p_T^{(i)}(\sin\phi_i,\cos\phi_i,\sinh\eta_i,\cosh\eta_i)
\eeqa 
For the NLO contribution $n=1$ and for the NNLO contribution $n=2$.

As far as phenomenology goes we have the maximally exclusive observable $\frac{d\sigma_{qq}}{d\eta_V d Q}$ from which we can
construct the observables. The transverse momentum of the vector boson is not an observable of this proces. Instead it is an observable
of $PP\rightarrow V + 1$ jet/parton. In the case of $PP\rightarrow V$ the bremstruhlung parton(s) are integrated out. We can either do 
this exclusive by imposing a transverse momentum cut or inclusive. The LO event can be showered using an unitary parton shower to make the region
integrated over exclusinve again while PQCD calculates the normalization of the event. In the exclusive case we can use a LL parton shower 
if the transverse momentum cut is sufficiently small. For the inclusive case one would want to use a matched shower.

\subsubsection{NNLO}


\subsection{$PP\rightarrow V+$ jet}
\subsubsection{NLO}
\subsubsection{NNLO}

\subsection{$PP\rightarrow 2$ jets}
\subsubsection{NLO}
\subsubsection{NNLO}

\subsection{$PP\rightarrow V+2$ jets}
\subsubsection{NLO}
\subsubsection{NNLO}

\subsection{$PP\rightarrow 3$ jets}
\subsubsection{NLO}
\subsubsection{NNLO}

 
\bibliographystyle{JHEP}
\bibliography{mcfm_fbps.bib}
\end{document}

%%  LocalWords:  hadronic
