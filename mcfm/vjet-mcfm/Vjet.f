      program Vjet
      implicit real*8 (a-h,o-z)
      double precision starttime,finishtime,sig(0:2),sd(0:2),chi(0:2)
      logical exclusive,fbps,plot,int_ptj,int_rapj,int_rapq,int_phij
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      common /integrate/ fbps,ptjt,rapjt,phijt,rapqV,iptype,
     .                   int_ptj,int_rapj,int_rapq,int_phij

      include 'mxpart.f'
      include 'nf.f'
      double precision p(mxpart,4),msq(-nf:nf,-nf:nf)
*
* input parameters
*
* number of jets and order
*
* if final state exclusive (exclusive=.true.)
* there are exactly njets clusters passing the
* jet selection cuts, all possible other clusters fail the jet selection cuts.
* if final state inclusive (exclusive=.false.)
* there is are least njets cluster passing the jet cuts, other clusters could
* pass the jet selection cuts. 
*
      njets=0
      nloop=1

      exclusive=.false.
      plot=.false.

* shots and iterations per sweep
*
      itmax1=10 ! init
      itmax2=10 ! main
      nshot1=1000000 !lo init
      nshot2=10000000 !lo main
      nshot3=10000000 !nlo init
      nshot4=100000000 !nlo main

*
* experimental jet cuts
*
!      do i=0,30 !eta
!       do i=0,100,1 !Pt


      do i=1,1

      fbps=.true.
      if (fbps) then
         int_ptj=.true.
         int_rapj=.true.
         int_rapq=.true.
         int_phij=.true.
         ptjt=500.0d0
         rapjt=-0.75d0
         rapqV=0.51d0
      endif

      etminj=50.0d0
      etmaxj=w/2.d0
      rapmaxj=2.0d0
      rapminj=0d0
*
      etminl=0d0
      etmis =0d0
      delrjl=0d0
      rapmaxl=2000.0d0
      rlepmin =0d0
      rlepmax =10d0
*
* vector boson type (ivec=0 -> w- + w+ , ivec=1 -> w- , ivec=2 -> w+
*                    ivec=3 -> z0 )
* resonance (ireson=0 -> breit wigner, ireson=1 -> narrow width)
*
      ivec=3
      ireson=1

* renormalization & factorization scale :
*     Q_ren=crenorm * scale
*     Q_fac=cfact   * scale
*     scale^2  = 1. (total invariant mass)^2
*                2. vector boson mass (dynamical)
*                3. vector boson mass (on-shell )
*                4. M(V)^2 + P_t(V)^2
*                5. leading jet Et
*                6. P_T(V)
*

      irenorm = 1
      crenorm = 1.0d0 !1d-3*(10.0d0**i)
      ifact  =irenorm
      cfact  =crenorm
*
* setup variables used in monte carlo
*
      call setup
*
* calculate cross section
*
      call cpu_time(starttime)
!      call cross(sig,sd,chi)
      call cpu_time(finishtime)
      p(1,1)=0d0
      p(1,2)=0d0
      p(1,3)=-100d0
      p(1,4)=-100d0
      p(2,1)=0d0
      p(2,2)=0d0
      p(2,3)=100d0
      p(2,4)=-100d0
      p(3,2)=100d0
      p(3,1)=0d0
      p(3,3)=0d0
      p(3,4)=100d0
      p(4,2)=-100d0
      p(4,1)=0d0
      p(4,3)=0d0
      p(4,4)=100d0

      call qqb_z(p,msq)
      do k=-nf,nf
         do j=-nf,nf
            write(*,*) k,j,msq(k,j)
         enddo
      enddo
*     
* output results and distributions
*
      write(*,30) svec,njets,sig(0),sd(0),chi(0)
   30 format(/,'  sigma(',a2,' + ',i2,' jets) = ',g12.5,'  +/-',g12.5,
     . ' pb ',' (p=',g12.5,')'/)
*
      enddo ! loop over ymin values
      end
*
************************************************************************ 
*

      subroutine  setup
      implicit none
      integer j
      character*128 namePDFset
      double precision aemmz,Gf,xw
      include 'types.f'
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'cplx.h'
      include 'masses.f'
      include 'zcouple.f'
      include 'ewcharge.f'
      include 'zprods_decl.f'
      include 'zcouple_cms.f'
      data Q(-5)/+0.333333333333333_dp/
      data Q(-4)/-0.666666666666667_dp/
      data Q(-3)/+0.333333333333333_dp/
      data Q(-2)/-0.666666666666667_dp/
      data Q(-1)/+0.333333333333333_dp/
      data Q(0)/+0._dp/
      data Q(+1)/-0.333333333333333_dp/
      data Q(+2)/+0.666666666666667_dp/
      data Q(+3)/-0.333333333333333_dp/
      data Q(+4)/+0.666666666666667_dp/
      data Q(+5)/-0.333333333333333_dp/
      data tau/1._dp,-1._dp,1._dp,-1._dp,1._dp,0._dp,-1._dp,1._dp,-1._dp,1._dp,-1._dp/
 
      namePDFset='/home/giele/mcfm-fbps/mcfm/lhapdf/share/LHAPDF/CT10nlo'
!'   
      call InitPDFset(namePDFset)
      call InitPDF(0)

      aemmz=7.7676170824406213e-3
      Gf=0.00001197450526084678
      xw=0.2228972225239183
      zmass= 91.1876
      wmass= 80.385
      zwidth= 2.4952
      wwidth= 2.085
      zesq=cplx2(fourpi*aemmz,0d0)
      zxw=cplx2(xw,0d0)
      zsin2w=two*sqrt(zxw*(cone-zxw))
      do j=1,nf
         zl(j)=(tau(j)-two*Q(j)*zxw)/zsin2w
         zr(j)=      (-two*Q(j)*zxw)/zsin2w
      enddo
      zle=(-cone-two*(-cone)*zxw)/zsin2w
      zre=(-ctwo*(-cone)*zxw)/zsin2w
      zln=(+cone-two*(+czip)*zxw)/zsin2w
      zrn=czip      
      q1=-1.0
      zl1=zle
      zr1=zre     

      return
      end
