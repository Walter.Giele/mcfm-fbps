      subroutine gen_njets(r,njets,p,wt,*)
      implicit none
      integer mxpart,mxdim
      parameter (mxpart=10)
      parameter (mxdim=10)
c---- generate phase space for 2-->2+n process
c---- with (34) being a vector boson and 5,..,4+n the jets
c---- r(mxdim),p1(4),p2(4) are inputs reversed in sign
c---- from physical values
c---- phase space for -p1-p2 --> p3+p4+p5+p6
c---- with all 2 pi's (ie 1/(2*pi)^(4+2n))
c----
c---- if 'nodecay' is true, then the vector boson decay into massless
c---- particles is not included and 2 less integration variables
c---- are required
      double precision r(mxdim),rdk1,rdk2
      double precision p(mxpart,4),p3(4),p34(4),psumjet(4),pcm(4),Q(4)
      double precision p1(4),p2(4),pjet(4)
      double precision wt,sqrts,xx(2),pi,xmin
      double precision hmin,hmax,delh,h,pt,etamax,etamin
      double precision y,sinhy,coshy,phi,mv2,wtbw,mjets
      double precision ybar,ptsumjet2,ycm,sumpst,q0st,rshat
      double precision costh,sinth,dely,xjac
      double precision ptjetmin,etajetmin,etajetmax,pbreak,Rmin
      double precision plstar,estar,plstarsq,y5starmax,y5starmin,mf,beta
      parameter(pi=3.141592653589793238d0)
      integer j,nu,njets,ijet
      logical xxerror,pass,part,nodecay
      common /Rcut/ Rmin
      data xxerror/.false./
      common /energy/ sqrts

      wt=1d0
      sqrts=7000d0
      nodecay=.false.
      xmin=0d0
      ptjetmin=20d0
      etajetmax=10d0
      mv2=(91.187)**2
      do nu=1,4
        do j=1,4+njets
          p(j,nu)=0d0
        enddo
        psumjet(nu)=0d0
        pcm(nu)=0d0
      enddo

      wt=2d0*pi

      do ijet=1,njets
        wt=wt/16d0/pi**3
        call genpt(r(ijet),ptjetmin,.false.,pt,xjac)
        wt=wt*xjac
        etamax=sqrts/2d0/pt
        if (etamax**2 .le. 1d0) then
          write(6,*) 'etamax**2 .le. 1d0 in gen_njets.f',etamax**2
          wt=0d0
          return 1
        endif
        etamax=dlog(etamax+dsqrt(etamax**2-1d0))

        etamax=min(etamax,etajetmax)
        y=etamax*(2d0*r(njets+ijet)-1d0)
        wt=wt*2d0*etamax

        sinhy=dsinh(y)
        coshy=dsqrt(1d0+sinhy**2)
        p(4+ijet,4)=pt*coshy

        phi=2d0*pi*r(2*njets+ijet)
        wt=wt*2d0*pi

        p(4+ijet,1)=pt*dcos(phi)
        p(4+ijet,2)=pt*dsin(phi)
        p(4+ijet,3)=pt*sinhy

        do nu=1,4
          psumjet(nu)=psumjet(nu)+p(4+ijet,nu)
        enddo
      enddo
c--- invariant mass of jets
      mjets=psumjet(4)**2-psumjet(1)**2-psumjet(2)**2-psumjet(3)**2
      mjets=dsqrt(dabs(mjets))

      ybar=0.5d0*dlog((psumjet(4)+psumjet(3))/(psumjet(4)-psumjet(3)))
      ptsumjet2=psumjet(1)**2+psumjet(2)**2
      plstarsq=((sqrts**2-mv2-mjets**2)**2
     . -4d0*(mjets**2*mv2+ptsumjet2*sqrts**2))/(4d0*sqrts**2)
      if (plstarsq .le. 0d0) then
        wt=0d0
        return 1
      endif
      plstar=dsqrt(plstarsq)
      Estar=dsqrt(plstarsq+ptsumjet2+mjets**2)
      if (abs(Estar/plstar-1d0) .lt. 1d-12) then
        wt=0d0
        return 1
      endif
      y5starmax=0.5d0*dlog((Estar+plstar)/(Estar-plstar))
      y5starmin=-y5starmax

      etamax=ybar-y5starmin
      etamin=ybar-y5starmax
      dely=etamax-etamin
      ycm=etamin+r(3*njets+2)*dely
      sinhy=dsinh(ycm)
      coshy=dsqrt(1d0+sinhy**2)

c--- now make the initial state momenta
      sumpst=ptsumjet2+(psumjet(3)*coshy-psumjet(4)*sinhy)**2
      q0st=dsqrt(mv2+sumpst)
      rshat=q0st+dsqrt(mjets**2+sumpst)
      pcm(4)=rshat*coshy
      pcm(3)=rshat*sinhy

      xx(1)=(pcm(4)+pcm(3))/sqrts
      xx(2)=(pcm(4)-pcm(3))/sqrts
c      write(6,*) plstar,Estar,plstarsq,ptsumjet2,mjets**2
c      pause

      if   ((xx(1)*xx(2) .gt. 1d0) .and. (xxerror .eqv. .false.)) then
        xxerror=.true.
c        write(6,*) 'gen_njets: xx(1)*xx(2),xx(1),xx(2)',
c     .   xx(1)*xx(2),xx(1),xx(2)
      endif

      if   ((xx(1) .gt. 1d0) .or. (xx(2) .gt. 1d0)
     & .or. (xx(1) .lt. xmin).or. (xx(2) .lt. xmin)) then
         wt=0d0
         return 1
      endif

      wt=wt*dely

      do j=1,4
        Q(j)=pcm(j)-psumjet(j)
      enddo

      p(1,4)=-xx(1)*sqrts/2d0
      p(1,3)=p(1,4)
      p(2,4)=-xx(2)*sqrts/2d0
      p(2,3)=-p(2,4)

      wt=wt*rshat/(sqrts**2*q0st)


      p(3,:)=Q(:)
      p(4,:)=0d0

      beta=1d0
      wt=wt*beta/8d0/pi

      p(4,:)=p(5,:)
      p(5,:)=p(6,:)

!added flux factor
      wt=wt/xx(1)/xx(2)/sqrts/sqrts

      return
      end
