      subroutine genfbps(r,npar,wtphase,pass)
      implicit none
      logical int_ptj,int_rapj,int_rapq,int_phij,fbps,lpass,exclusive
      integer ndim,iptype,i,ifact,irenorm,ivec,nloop,ireson,istruc,njets
      integer npar,pass,type,j,sector,b
      integer choosecutoff
      double precision wtphase,a,Obs,pi,t0,ymin,smin,s0,s,sqsab
      double precision ptminj,ptmaxj,Rmin,rapmaxj,rapminj,etamaxj
      double precision etamaxq,phij,r1,r2,rap,rapj
      double precision pt,ptj,rapq,tborn,tot,wtborn
      double precision xa,xb,ptjet,eta,dot
      double precision ptjt,rapjt,phijt,rapqV,rn
      double precision rm,facscale,renscale,as
      double precision rmnlep,rmv,rgv,rmv2,rmg,cfact,crenorm
      double precision sqrts,sab,ya1,yb1,ya2,yb2,y12
      double precision breitmax,breitmin,breitwgt,sba,siv
      complex*16 zu,zd,z1i,z1j,z2i,z2j
      double precision p1(4),p2(4),pj(4),Q(4),pl1(4),pl2(4)
      double precision r(*),rb(3),p(10,4),ph(10,4),ppar(4,10)
      parameter(pi=3.141592653589793238d0)
      common /fraction/ rm,xa,xb,facscale,renscale,as
      common /jetdef/ ptminj,ptmaxj,Rmin,rapmaxj,rapminj
      common /parmom/ ppar
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     .                ivec,ireson
      common /invar/ sab,ya1,yb1,ya2,yb2,y12
      common /integrate/ fbps,ptjt,rapjt,phijt,rapqV,iptype,
     .                   int_ptj,int_rapj,int_rapq,int_phij
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt
      common /thcut/ smin,s0,ymin,choosecutoff
      common /invariant/ sba,siv(10,10)
      common /spinor/ zu(10,10),zd(10,10)
      common /normal/exclusive,sector,type
      pass=0
      ndim=1
      wtphase=1d0
      iptype=2

      t0=1d-8
      if (npar.ge.1) then
         if (int_ptj) then
            r1=r(ndim)
            ndim=ndim+1
            call pick(iptype,ptj,ptminj,ptmaxj,r1,wtphase)
         else
            ptj=ptjt
         endif
         a=dsqrt(ptj**2+rm**2)
         if (int_rapj) then
            r1=r(ndim)
            ndim=ndim+1
            etamaxj=min(dlog(sqrts/ptj),rapmaxj)
            rapj=etamaxj*(-1d0+2d0*r1)
            wtphase=wtphase*2d0*etamaxj
         else
            rapj=rapjt
         endif
         if (int_rapq) then
            r1=r(ndim)
            ndim=ndim+1
            etamaxq=dlog(sqrts/a)
            rapq=etamaxq*(-1d0+2d0*r1)
            wtphase=wtphase*2d0*etamaxq
         else
            rapq=rapqV
         endif
         if (int_phij) then
             r1=rn(1)
            !r1=r(ndim)
            !ndim=ndim+1
            phij=2d0*pi*r1
            wtphase=wtphase*2d0*pi
         else
            phij=phijt
         endif

         pj(1)=ptj*dcos(phij)
         pj(2)=ptj*dsin(phij)
         pj(3)=ptj*dsinh(rapj)
         pj(4)=ptj*dcosh(rapj)

         Q(1)=-pj(1)
         Q(2)=-pj(2)
         Q(3)=a*dsinh(rapq)
         Q(4)=a*dcosh(rapq)
         xa=(ptj*dexp(rapj)+a*dexp(rapq))/sqrts
         xb=(ptj*exp(-rapj)+a*exp(-rapq))/sqrts
         if ((max(xa,xb).gt.1d0).or.(min(xa,xb).lt.0d0)) return

         p1(1)=0d0
         p1(2)=0d0
         p1(3)=0.5d0*sqrts*xa
         p1(4)=p1(3)

         p2(1)=0d0
         p2(2)=0d0
         p2(3)=-0.5d0*sqrts*xb
         p2(4)=-p2(3)

         wtphase=wtphase*(ptj/sqrts**2)/(8.0d0*pi**2)
         wtphase=wtphase/xa/xb
         r1=rn(1)
         r2=rn(1)
!         r1=0.51d0
!         r2=0.88d0
         call decayV(Q,r1,r2,pl1,pl2,wtphase)
         ppar(:,1)=p1(:)
         ppar(:,2)=p2(:)
         ppar(:,3)=pl1(:)
         ppar(:,4)=pl2(:)
         ppar(:,5)=pj(:)

         sab=xa*xb*sqrts**2
         ya1=2d0*dot(p1,pj)/sab
         yb1=2d0*dot(p2,pj)/sab

         if (npar.eq.2) then
            p(1,:)=p1(:)
            p(2,:)=p2(:)
            p(3,:)=pl1(:)
            p(4,:)=pl2(:)
            p(5,:)=pj(:)
            lpass=.true.
            rb(1)=r(ndim)
            rb(2)=r(ndim+1)
            rb(3)=r(ndim+2)
            call genparton(p,rb,t0,type,ph,wtphase,lpass,b)
            if(.not.lpass) return
            sab=2d0*dot(ph(1,:),ph(2,:))
            xa=2d0*ph(1,4)/sqrts
            xb=2d0*ph(2,4)/sqrts

            if(choosecutoff.eq.2) ymin=smin/sab
!            ymin=smin/sab
!            ymin=1e-3
            ppar(:,1)=ph(1,:)
            ppar(:,2)=ph(2,:)
            ppar(:,3)=ph(3,:)
            ppar(:,4)=ph(4,:)
            if (b.eq.3) then
!            if (rn(1.).gt.0.5d0) then
               ppar(:,5)=ph(5,:)
               ppar(:,6)=ph(6,:)
            else
               ppar(:,5)=ph(6,:)
               ppar(:,6)=ph(5,:)
            endif
         endif
      endif

*
* construct invariant mass & phase angle matrices
*
      if(choosecutoff.eq.1) smin=ymin*sab ! ymin is cutoff 
      do 10 i=1,npar+3
         do 10 j=i+1,npar+4
            siv(i,j)=2d0*(ppar(4,i)*ppar(4,j)-ppar(1,i)*ppar(1,j)
     .                   -ppar(2,i)*ppar(2,j)-ppar(3,i)*ppar(3,j))/sab
            siv(j,i)=siv(i,j)
 10   continue
      sba=sab
      if (npar.eq.2) then
         ya1=siv(1,5)
         ya2=siv(2,5)
         yb1=siv(1,6)
         yb2=siv(2,6)
         y12=siv(5,6)

      
         if (ya1.lt.ymin) return
         if (ya2.lt.ymin) return
         if (yb2.lt.ymin) return
         if (yb1.lt.ymin) return
         if (y12.lt.ymin) return
      endif
      do 20 j=3,npar+4
         siv(1,j)=-siv(1,j)
         siv(j,1)=-siv(j,1)
         siv(2,j)=-siv(2,j)
         siv(j,2)=-siv(j,2)
 20   continue
      sqsab=sqrt(abs(sab))
      do 30 i=1,npar+3
         do 30 j=i+1,npar+4
            s=siv(i,j)
            z2i=csqrt(cmplx(ppar(4,i)-ppar(1,i)))
            z1i=(ppar(2,i)-(0d0,1d0)*ppar(3,i))/z2i
            z2j=csqrt(cmplx(ppar(4,j)-ppar(1,j)))
            z1j=(ppar(2,j)-(0d0,1d0)*ppar(3,j))/z2j
            zu(i,j)=(z2i*z1j-z2j*z1i)/sqsab
            zd(i,j)=conjg(zu(i,j))
            if (s.le.0) then
               zu(i,j)=cmplx(0d0,1d0)*zu(i,j)
               zd(i,j)=cmplx(0d0,1d0)*zd(i,j)
            endif
            zu(j,i)=-zu(i,j)
            zd(j,i)=-zd(i,j)
 30   continue
      zu(1,2)=-zu(1,2)
      zd(1,2)=-zd(1,2)
      zu(2,1)=-zu(2,1)
      zd(2,1)=-zd(2,1)
*

      pass=1
      return
      end



      subroutine genparton(p,r,t0,type,ph,wt,pass,b)
      implicit none
      logical pass,momcheck,cluster,exclusive
      integer mxpart,b,clust,nj,type,sector
      parameter(mxpart=10)
      double precision r1,r2,r3,wt,t0,DOT
      double precision p1(4),p2(4),pj(4),Q(4),v(4),pl1(4),pl2(4)
      double precision pah(4),pbh(4),Qh(4),p1h(4),p2h(4),pl1h(4),pl2h(4)
      double precision p(mxpart,4),ph(mxpart,4),pclust(mxpart,4),r(3)
!
!     Radiate an additional massless partice (from p_branch).
!     using p1+p2 --> Q+pj to generate ph1+ph2 --> Qh+ph1+ph2 

      momcheck=.false.
      p1(:)=p(1,:)
      p2(:)=p(2,:)
      Q(:) =p(3,:)+p(4,:)
      pl1(:)=p(3,:)
      pl2(:)=p(4,:)
      pj(:)=p(5,:)


      if (type.gt.0) b=1+int(rand()*4)
      if (type.eq.0) b=3+int(rand()*2)
      if (type.lt.0) b=1+int(rand()*2)
      if (b.le.2) then
         if (abs(type).eq.1) then
            call gen_init1(b,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass)
            call boostx(pl1,Q,Qh,pl1h)
            call boostx(pl2,Q,Qh,pl2h)
            sector=0
         endif
         if (abs(type).eq.2) then
            call gen_init2(b,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass)
            pl1h(:)=pl1(:)
            pl2h(:)=pl2(:)
            sector=0
         endif
      else
         call gen_final(b-2,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass) 
         pl1h(:)=pl1(:)
         pl2h(:)=pl2(:)
         wt=wt*2d0
         sector=1
      endif
      ph(1,:)=pah(:)
      ph(2,:)=pbh(:)
      ph(3,:)=pl1h(:)
      ph(4,:)=pl2h(:)
      ph(5,:)=p1h(:)
      ph(6,:)=p2h(:)

      if (momcheck.and.pass) then
         write(*,*) '-----------------------------------------------',wt
         write(*,*) 'p1',p1,dot(p1,p1)
         write(*,*) 'p2',p2,dot(p2,p2)
         write(*,*) 'Q',Q,sqrt(dot(Q,Q))
         write(*,*) 'pl1 ',pl1,dot(pl1,pl1)
         write(*,*) 'pl2 ',pl2,dot(pl2,pl2)
         write(*,*) 'pj',pj,dot(pj,pj)
         write(*,*) '++',p1+p2-pl1-pl2-pj
         write(*,*)
         write(*,*)
         write(*,*) 'pah',pah,dot(pah,pah)
         write(*,*) 'pbh',pbh,dot(pbh,pbh)
         write(*,*) 'Qh',Qh,sqrt(dot(Qh,Qh))
         write(*,*) 'pl1h ',pl1h,sqrt(abs(dot(pl1h,pl1h)))
         write(*,*) 'pl2h ',pl2h,sqrt(abs(dot(pl2h,pl2h)))
         write(*,*) 'p1h',p1h,dot(p1h,p1h)
         write(*,*) 'p2h',p2h,dot(p2h,p2h)
         write(*,*) '++',pah+pbh-pl1h-pl2h-p1h-p2h
         write(*,*)
         write(*,*)
      endif




      return
      end

      subroutine gen_init1(b,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass)
      implicit none
      logical pass
      integer mxpart,b
      parameter(mxpart=10)
      double precision wt,t0,xa,xb,ta,tb,Jac,r1,r2,r3,tmax,p2tDQ
      double precision phi,pi,pt,al,xah,xbh,p2DQ,dot,pt2,pl2
      double precision rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt
      parameter(pi=3.141592653589793238d0)
      double precision Q(4),pab(4),Qh(4),pah(4),pbh(4),p1h(4),p2h(4)
      double precision p1(4),p2(4),pj(4),r(3)
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt

      pass=.false.
      pab=p1+p2
      xa=(pab(4)+pab(3))/sqrts
      xb=(pab(4)-pab(3))/sqrts

!     pick tau_a and tau_b of emitted particle
      r1=r(1)
      r2=r(2)
      r3=r(3)
      tmax=sqrts
      if (tmax.lt.t0) return
      if (b.eq.1) then
         call pick(2,tb,t0,tmax,r1,wt)
         call pick(2,ta,t0,tmax,r2,wt)
      endif
      if (b.eq.2) then
         call pick(2,ta,t0,tmax,r1,wt)
         call pick(2,tb,t0,tmax,r2,wt)
      endif
      wt=0.25d0*wt   
      call pick(1,phi,0d0,2d0*pi,r3,wt)
!     construct p2h
      pt=sqrt(abs(ta*tb))
      p2h(4)=0.5*(ta+tb)
      p2h(3)=0.5*(ta-tb)
      p2h(2)=pt*sin(phi)
      p2h(1)=pt*cos(phi)
!     jet momentum is left unaltered
      p1h=pj
!     construct intial state particles
      p2DQ=p2h(4)*Q(4)-p2h(3)*Q(3)
      p2tDQ=-(p2h(1)*Q(1)+p2h(2)*Q(2))
      pl2=pt**2
      Jac=pl2**2+2d0*pl2*p2tDQ+p2DQ**2
      al=(p2DQ-pl2-sqrt(abs(Jac)))/pl2
      Pab(4)=Pab(4)-al*p2h(4)
      Pab(3)=Pab(3)-al*p2h(3)
      Pab(2)=0d0
      Pab(1)=0d0
      xah=(Pab(4)+Pab(3))/sqrts
      xbh=(Pab(4)-Pab(3))/sqrts
      if ((xah.gt.1d0).or.(xbh.gt.1d0)) return
      pah(4)=0.5*sqrts*xah
      pah(3)=0.5*sqrts*xah
      pah(2)=0d0
      pah(1)=0d0
      pbh(4)=0.5*sqrts*xbh
      pbh(3)=-0.5*sqrts*xbh
      pbh(2)=0d0
      pbh(1)=0d0
!     construct Qhat
      Qh(4)=Q(4)-(al+1d0)*p2h(4)
      Qh(3)=Q(3)-(al+1d0)*p2h(3)
      Qh(2)=Q(2)-p2h(2)
      Qh(1)=Q(1)-p2h(1)
!     calculate Jacobian
      p2DQ=p2h(4)*Qh(4)-p2h(3)*Qh(3)
      p2tDQ=-(p2h(1)*Qh(1)+p2h(2)*Qh(2))
      Jac=sqrt(abs((pl2**2-2d0*pl2*p2tDQ+p2DQ**2)/Jac))

      wt=wt*Jac/(2d0*pi)**3
!     Change flux factor
      wt=wt*(xa/xah)*(xb/xbh)
      pass=.true.

      return
      end

      subroutine gen_init2(b,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass)
      implicit none
      logical pass
      integer mxpart,b
      parameter(mxpart=10)
      double precision wt,t0,xa,xb,ta,tb,Jac,r1,r2,r3,tmax,p2tDpj
      double precision phi,pi,pt,al,xah,xbh,p2Dpj,dot,pt2,pl2
      double precision rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt
      parameter(pi=3.141592653589793238d0)
      double precision Q(4),pab(4),Qh(4),pah(4),pbh(4),p1h(4),p2h(4)
      double precision p1(4),p2(4),pj(4),r(3)
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt

      pass=.false.
      pab=p1+p2
      xa=(pab(4)+pab(3))/sqrts
      xb=(pab(4)-pab(3))/sqrts

!     pick tau_a and tau_b of emitted particle
      r1=r(1)
      r2=r(2)
      r3=r(3)
      tmax=sqrts
      if (tmax.lt.t0) return
      if (b.eq.1) then
         call pick(2,tb,t0,tmax,r1,wt)
         call pick(2,ta,t0,tmax,r2,wt)
      endif
      if (b.eq.2) then
         call pick(2,ta,t0,tmax,r1,wt)
         call pick(2,tb,t0,tmax,r2,wt)
      endif
      wt=0.25d0*wt   
      call pick(1,phi,0d0,2d0*pi,r3,wt)
!     construct p2h
      pt=sqrt(abs(ta*tb))
      p2h(4)=0.5*(ta+tb)
      p2h(3)=0.5*(ta-tb)
      p2h(2)=pt*sin(phi)
      p2h(1)=pt*cos(phi)
! Vector boson momentum is left unaltered     
      Qh=Q
!     construct intial state particles
      p2Dpj=p2h(4)*pj(4)-p2h(3)*pj(3)
      p2tDpj=-(p2h(1)*pj(1)+p2h(2)*pj(2))
      pl2=pt**2
      Jac=pl2**2+2d0*pl2*p2tDpj+p2Dpj**2
      al=(p2Dpj-pl2-sqrt(abs(Jac)))/pl2
      Pab(4)=Pab(4)-al*p2h(4)
      Pab(3)=Pab(3)-al*p2h(3)
      Pab(2)=0d0
      Pab(1)=0d0
      xah=(Pab(4)+Pab(3))/sqrts
      xbh=(Pab(4)-Pab(3))/sqrts
      if ((xah.gt.1d0).or.(xbh.gt.1d0)) return
      pah(4)=0.5*sqrts*xah
      pah(3)=0.5*sqrts*xah
      pah(2)=0d0
      pah(1)=0d0
      pbh(4)=0.5*sqrts*xbh
      pbh(3)=-0.5*sqrts*xbh
      pbh(2)=0d0
      pbh(1)=0d0
!     construct the jet momentum p1h
      p1h(4)=pj(4)-(al+1d0)*p2h(4)
      p1h(3)=pj(3)-(al+1d0)*p2h(3)
      p1h(2)=pj(2)-p2h(2)
      p1h(1)=pj(1)-p2h(1)
!     calculate Jacobian
      p2Dpj=p2h(4)*p1h(4)-p2h(3)*p1h(3)
      p2tDpj=-(p2h(1)*p1h(1)+p2h(2)*p1h(2))
      Jac=sqrt(abs((pl2**2-2d0*pl2*p2tDpj+p2Dpj**2)/Jac))

      wt=wt*Jac/(2d0*pi)**3
!     Change flux factor
      wt=wt*(xa/xah)*(xb/xbh)
      pass=.true.

      return
      end

      subroutine gen_final(b,p1,p2,Q,pj,r,t0,pah,pbh,Qh,p1h,p2h,wt,pass)
      implicit none
      logical pass
      integer b
      integer choosecutoff
      double precision pi,sqrts,t0,t1,t2,tmax,x,wt,dot,phi,pt2,al,Jac
      double precision paDpb,pjDpa,pjDpb,pjbDn1,pjbDpa,pjbDpj,pjDpt
      double precision pjDpl,plDp2h,pl2,smin,s0,ymin
      double precision exp1,sqarg,a,bm,bp,s12,xa,xb,xah,xbh
      double precision rmnlep,rmv,rgv,rmv2,rmg,
     .                breitmax,breitmin,breitwgt
      double precision r(3),pa(4),pb(4),Q(4),p(4),p1(4),p2(4),pjb(4)
      double precision pah(4),pbh(4),Qh(4),p1h(4),p2h(4),n1(4),n2(4)
      double precision pt(4),pj(4),pl(4),p12(4),pab(4)
      parameter(pi=3.141592653589793238d0)
      common /setpha/ rmnlep,rmv,rgv,rmv2,rmg,sqrts,
     .                breitmax,breitmin,breitwgt
      common /thcut/ smin,s0,ymin,choosecutoff

      pass=.false.
      Qh(:)=Q(:)
      xa=2d0*p1(4)/sqrts
      xb=2d0*p2(4)/sqrts
!      write(*,*) xa,xb
      if (b.eq.1) then
         pa(:)=p1(:)
         pb(:)=p2(:)
      else
         pa(:)=p2(:)
         pb(:)=p1(:)
      endif
      
* caculate kinematics and construct momenta     
      pjDpa=dot(pj,pa)
      pjDpb=dot(pj,pb)
      paDpb=dot(pa,pb)
      tmax=2d0*paDpb
!      if (tmax.lt.t0) return
!      t0=0.25d0*smin/pa(4)
      call pick(2,t2,t0,tmax,r(1),wt)
!      t0=0.25d0*smin/pj(4)
      call pick(2,t1,t0,tmax,r(2),wt)
      call pick(1,phi,0d0,2d0*pi,r(3),wt)
      wt=0.5d0*wt

      n1(:)=pjDpb*pa(:)-pjDpa*pb(:)+paDpb*pj(:)
      n1(:)=n1(:)/sqrt(abs(dot(n1,n1)))

      pjb(1)=pj(2)
      pjb(2)=pj(3)
      pjb(3)=pj(1)
      pjb(4)=pj(4)
      pjbDn1=dot(pjb,n1)
      pjbDpj=dot(pjb,pj)
      pjbDpa=dot(pjb,pa)
      n2(:)=-pjDpa*pjbDn1*n1(:)+pjbDpj*pa(:)+pjbDpa*pj(:)-pjDpa*pjb(:)
      n2(:)=n2(:)/sqrt(abs(dot(n2,n2)))
      pt2=2d0*t1*t2*pa(4)*pj(4)/pjDpa
      pt(:)=sqrt(pt2)*(cos(phi)*n1(:)+sin(phi)*n2(:))
      p1h(:)=(t1*pa(4)*pj(:)+t2*pj(4)*pa(:))/pjDpa+pt(:)

      pt(1)=p1h(1)
      pt(2)=p1h(2)
      pt(3)=0d0
      pt(4)=0d0
      pl(1)=0d0
      pl(2)=0d0
      pl(3)=pj(3)
      pl(4)=pj(4)
      pl2=dot(pl,pl)
      pjDpl=dot(pl,p1h)
      pjDpt=dot(pt,pj)
      exp1=pl2+2d0*pjDpt
      sqarg=pjDpl**2+exp1*pl2
         
      bm=(pjDpl-sqrt(sqarg))/pl2
      bp=(pjDpl+sqrt(sqarg))/pl2
      p2h(1)=pj(1)-p1h(1)
      p2h(2)=pj(2)-p1h(2)
      p2h(3)=bp*pj(3)-p1h(3)
      p2h(4)=bp*pj(4)-p1h(4)
      pab(:)=pa(:)+pb(:)-(1d0-bp)*pl(:)
      xah=(Pab(4)+Pab(3))/sqrts
      xbh=(Pab(4)-Pab(3))/sqrts
      if ((xah.ge.1d0).or.(xbh.ge.1d0)) return
      pah(4)=0.5*sqrts*xah
      pah(3)=0.5*sqrts*xah
      pah(2)=0d0
      pah(1)=0d0
      pbh(4)=0.5*sqrts*xbh
      pbh(3)=-0.5*sqrts*xbh
      pbh(2)=0d0
      pbh(1)=0d0
*     calculate the event weight
      pt2=(pj(1)**2+pj(2)**2)
      Jac=sqrt(pl2*pt2)*bp/(pl2*abs(bp-bm))
      Jac=1d0/abs(1d0-bm/bp)
      wt=wt*Jac/(2d0*pi)**3
!     Change flux factor
      wt=wt*(xa/xah)*(xb/xbh)
      pass=.true.

      return
      end

      subroutine pick(itype,s,smin,smax,r1,wt)
      implicit real*8(a-h,o-z)
*
* itype=1  pick linearly
* itype=2  pick logarithmically
*
      if (smax.lt.smin) then
         write(*,*) '<<<',smin,smax
         stop
      endif
      if(itype.eq.1)then
        s =smin+r1*(smax-smin)
        wt=wt*(smax-smin)
      endif
      if(itype.eq.2)then
        s =smin*(smax/smin)**r1
        wt=wt*log(smax/smin)*s
      endif
      return
      end

      function dot(a,b)
      implicit none
      double precision dot,a(4),b(4)

      dot=a(4)*b(4)-a(3)*b(3)-a(2)*b(2)-a(1)*b(1)

      return
      end

      subroutine decayV(Q,rdk1,rdk2,p1,p2,wt)
      implicit none
      character*4 hdecaymode
      double precision rdk1,rdk2,beta,costh,phi,sinth,wt
      double precision mv2,mf,mtau,mb,dot,pi
      double precision Q(4),p1(4),p2(4),p34(4)
      parameter(pi=3.141592653589793238d0)
      parameter (hdecaymode='')

      mv2=dot(Q,Q)
      if (hdecaymode == 'tlta') then
         mf=mtau
      elseif (hdecaymode == 'bqba') then
         mf=mb
      else
         mf=0d0
      endif

c---  decay boson into leptons, in boson rest frame
      if (mv2 > 4d0*mf**2) then
         beta=sqrt(1d0-4d0*mf**2/mv2)
      else
         beta=0d0
      endif
      costh=2d0*rdk1-1d0
      sinth=dsqrt(1d0-costh**2)
      phi=2d0*pi*rdk2
      p34(4)=dsqrt(mv2)/2d0
      p34(1)=beta*p34(4)*sinth*dcos(phi)
      p34(2)=beta*p34(4)*sinth*dsin(phi)
      p34(3)=beta*p34(4)*costh

      
c---  boost into lab frame    
      call boostV(dsqrt(mv2),Q,p34,p1)
      p2(:)=Q(:)-p1(:)

      wt=wt*beta/8d0/pi
      return
      end

      subroutine boostV(mass,p1,p_in,p_out)
c     take momenta p_in in frame in which particle one is at rest with mass 
c     "mass" 
c     and convert to frame in which particle one has fourvector p1
      implicit none
      double precision mass,p1(4),p_in(4),p_out(4)
      double precision gam,beta(1:3),bdotp,one
      parameter(one=1d0)
      integer j,k
      gam=p1(4)/mass
      bdotp=0d0
      do j=1,3
      beta(j)=-p1(j)/p1(4)
      bdotp=bdotp+p_in(j)*beta(j)
      enddo
      p_out(4)=gam*(p_in(4)-bdotp)
      do k=1,3
      p_out(k)=p_in(k)+gam*beta(k)*(gam/(gam+one)*bdotp-p_in(4))
      enddo
      return
      end      

      subroutine boostx(p_in,pt,ptt,p_out)
      implicit none
c--- Boost input vector p_in to output vector p_out using the same
c--- transformation as required to boost massive vector pt to ptt
      double precision p_in(4),pt(4),ptt(4),p_out(4),
     . p_tmp(4),beta(3),mass,gam,bdotp
      integer j
    
      mass=pt(4)**2-pt(1)**2-pt(2)**2-pt(3)**2  
      if (mass .lt. 0d0) then
        write(6,*) 'mass**2 .lt. 0 in boostx.f, mass**2=',mass
        stop
      endif
      mass=dsqrt(mass)

c--- boost to the rest frame of pt
      gam=pt(4)/mass

      bdotp=0d0
      do j=1,3
        beta(j)=-pt(j)/pt(4)
        bdotp=bdotp+beta(j)*p_in(j)
      enddo
      p_tmp(4)=gam*(p_in(4)+bdotp)
      do j=1,3
        p_tmp(j)=p_in(j)+gam*beta(j)/(1d0+gam)*(p_in(4)+p_tmp(4))
      enddo     

c--- boost from rest frame of pt to frame in which pt is identical
c--- with ptt, thus completing the transformation          
      gam=ptt(4)/mass

      bdotp=0d0
      do j=1,3
        beta(j)=+ptt(j)/ptt(4)
        bdotp=bdotp+beta(j)*p_tmp(j)
      enddo
      p_out(4)=gam*(p_tmp(4)+bdotp)
      do j=1,3
        p_out(j)=p_tmp(j)+gam*beta(j)/(1d0+gam)*(p_out(4)+p_tmp(4))
      enddo

      return
      end
      

