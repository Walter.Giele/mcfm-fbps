************************************************************************ 
*                                                                       
*
*      p pbar -> V + 0 or 1 jets  at up to O(alphas**2)                 
*
*                                                                       
*
*      RELEASE 4.1, 17/07/97                                            
*
*                                                                       
*
************************************************************************ 

      program dyrad
      implicit real*8 (a-h,o-z)
      double precision starttime,finishtime,sig(0:2),sd(0:2),chi(0:2)
      logical exclusive,fbps,plot,int_ptj,int_rapj,int_rapq,int_phij
      integer sector,type
      integer intmethod ! method of integration 1 = sauve, 2 = vegas 
      character*5 svec,spp1,spp2
      character*128 sstru,sir,sreson,sex,sjet
      character*128 namePDFset
      integer choosecutoff
      double precision sminseq(10)
      common /phypar/ w,ipp1,ipp2,rmw,rgw,rmz,rgz,sw2,qcdl
      common /inppar/ njets,nloop,istruc,irenorm,ifact,crenorm,cfact,
     . ivec,ireson
      common /shots/ itmax1,itmax2,nshot1,nshot2,nshot3,nshot4
      common /jetdef/ etminj,etmaxj,delrjj,rapmaxj,rapminj 
      common /clusdef/ rsep,jalg1,jalg2
      common /thcut/ smin,s0,ymin,choosecutoff 
      common /lepcut/ etminl,delrjl,rapmaxl,etmis,rlepmin,rlepmax,raphad
      common /koppel/ nf,b0,cru,clu,crd,cld,crl,cll,
     .                cru2,clu2,crd2,cld2,crl2,cll2
      common /normal/exclusive,sector,type,b
      common /order/iorder
      common /plots/plot
      common /integrate/ fbps,ptjt,rapjt,phijt,rapqV,iptype,
     .                   int_ptj,int_rapj,int_rapq,int_phij
      common /integration1/ intmethod

      data sminseq /100d0,50d0,20d0,10d0,5d0,2d0,1d0,0.5d0,0.2d0,0.1d0/
*
* input parameters
*
* number of jets and order
*
* if final state exclusive (exclusive=.true.)
* there are exactly njets clusters passing the
* jet selection cuts, all possible other clusters fail the jet selection cuts.
* if final state inclusive (exclusive=.false.)
* there is are least njets cluster passing the jet cuts, other clusters could
* pass the jet selection cuts. 
*
      intmethod=1
      
!      choosecutoff=1 ! ymin is cutoff
      choosecutoff=2 ! smin is cutoff 
      if (choosecutoff.lt.0 .or. choosecutoff.gt.2) stop 
      njets=1
      nloop=1
      exclusive=.false.
      plot=.false.

      namePDFset='CT14nlo'   
*      namePDFset='/Users/terrancefigy/lhapdf-install/share/LHAPDF/MSTW2008nlo68cl'
      call InitPDFsetByName(namePDFset)
      call InitPDF(0)
      
      
* shots and iterations per sweep
*
      itmax1=10 ! init
      itmax2=10 ! main
      nshot1=1000000 !lo init
      nshot2=10000000 !lo main
      nshot3=10000000 !nlo init
      nshot4=100000000 !nlo main

! controlling radiation option: 
!   type= 2 keeps vector boson momentum invariant for initial state radiation
!   type= 1 keeps jet momentum invariant for initial state radiation
!   type= 0 only final state radiation
!   type=-1 only initial state radiation of type 1
!   type=-2 only initial state radiation of type 2
      type=0

      if (.not.(exclusive)) then
         write(*,*) 'Inclusive, only final state branching performed'
         type=0
         sector=1
      endif

*
* experimental jet cuts
*
!      do i=0,30 !eta
!       do i=0,100,1 !Pt


      do i=1,10

      w=14000.0d0
      etminj=50.0d0
      etmaxj=w/2.d0
      rapmaxj=2.0d0
      rapminj=0d0
      fbps=.true.
!      fbps=.false.


      int_ptj=.true.
      int_rapj=.true.
      int_rapq=.true.
      int_phij=.true.

      ptjt=500.0d0
!      ptjt=20.0d0+10.0*dble(i)
!      print*,ptjt
      rapjt=-0.75d0
!      rapjt=0.1d0*i
      rapqV=0.51d0
!      phijt=0.20d0

*
* clustering criterion
*       jalg1 = 1 ; deltaR(i,j)   < delrjj
*       jalg1 = 2 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*       jalg1 = 3 ; kt algorithm; R = delrjj
*       jalg1 = 4 ; deltaR(i,jet) < delrjj and deltaR(j,jet) < delrjj
*                      but deltaR(i,j) < Rsep
*
      jalg1=3
* recombination scheme
*       jalg2 = 1 is D0 eta/phi
*       jalg2 = 2 is Snowmass
*       jalg2 = 3 is 4 momentum - ET = sqrt(px**2+py**2)
*       jalg2 = 4 is 4 momentum - ET = E sin(theta)
*
      jalg2=3
* conesize
      delrjj=0.4d0      
*
*     if jalg1 = 4, must set rsep
*
      rsep=1.3d0*delrjj
*
* experimental lepton cuts
*
      etminl=0d0
      etmis =0d0
      delrjl=0d0
      rapmaxl=2000.0d0
      rlepmin =0d0
      rlepmax =10d0
*
* hadron rapidity coverage (missing E_t reconstruction)
*
      raphad=4d0
*
* theoretical cut
*
!     smin=0.5d0*(ptjt/250d0)**2
!     ymin=1d-4
!      ymin=10.d0**(-1.0d0 - 0.2d0*i)
!      smin=10.d0**(2.0d0-0.2d0*i)
      smin=sminseq(i)
!      smin = 1.0d0 
!      ymin=10d0**(-i/6d0)
!      smin=ymin
      safety=1d3           
      s0=smin/safety        
*
* vector boson type (ivec=0 -> w- + w+ , ivec=1 -> w- , ivec=2 -> w+
*                    ivec=3 -> z0 )
* resonance (ireson=0 -> breit wigner, ireson=1 -> narrow width)
*
      ivec=3
      ireson=1
*
* structure functions 
*
* istruc=1  -> mrse        (msbar), Lambda = 0.1  GeV
* istruc=2  -> mrsb        (msbar), Lambda = 0.2  GeV
* istruc=3  -> kmrshb      (msbar), Lambda = 0.19 GeV
* istruc=4  -> kmrsb0      (msbar), Lambda = 0.19 GeV
* istruc=5  -> kmrsb-      (msbar), Lambda = 0.19 GeV
* istruc=6  -> kmrsh- (5)  (msbar), Lambda = 0.19 GeV
* istruc=7  -> kmrsh- (2)  (msbar), Lambda = 0.19 GeV
* istruc=8  -> mts1        (msbar), Lambda = 0.2  GeV
* istruc=9  -> mte1        (msbar), Lambda = 0.2  GeV
* istruc=10 -> mtb1        (msbar), Lambda = 0.2  GeV
* istruc=11 -> mtb2        (msbar), Lambda = 0.2  GeV
* istruc=12 -> mtsn1       (msbar), Lambda = 0.2  GeV
* istruc=13 -> kmrss0      (msbar), Lambda = 0.215 GeV
* istruc=14 -> kmrsd0      (msbar), Lambda = 0.215 GeV
* istruc=15 -> kmrsd-      (msbar), Lambda = 0.215 GeV
* istruc=16 -> mrss0       (msbar), Lambda = 0.230 GeV
* istruc=17 -> mrsd0       (msbar), Lambda = 0.230 GeV
* istruc=18 -> mrsd-       (msbar), Lambda = 0.230 GeV
* istruc=19 -> cteq1l      (msbar), Lambda = 0.168 GeV
* istruc=20 -> cteq1m      (msbar), Lambda = 0.231 GeV
* istruc=21 -> cteq1ml     (msbar), Lambda = 0.322 GeV
* istruc=22 -> cteq1ms     (msbar), Lambda = 0.231 GeV
* istruc=23 -> grv         (msbar), Lambda = 0.200 GeV
* istruc=24 -> cteq2l      (msbar), Lambda = 0.190 GeV
* istruc=25 -> cteq2m      (msbar), Lambda = 0.213 GeV
* istruc=26 -> cteq2ml     (msbar), Lambda = 0.322 GeV
* istruc=27 -> cteq2ms     (msbar), Lambda = 0.208 GeV
* istruc=28 -> cteq2mf     (msbar), Lambda = 0.208 GeV
* istruc=29 -> mrsh        (msbar), Lambda = 0.230 GeV
* istruc=30 -> mrsa        (msbar), Lambda = 0.230 GeV
* istruc=31 -> mrsg        (msbar), Lambda = 0.255 GeV
* istruc=32 -> mrsap       (msbar), Lambda = 0.231 GeV
* istruc=33 -> grv94       (msbar), Lambda = 0.200 GeV
* istruc=34 -> cteq3l      (msbar), Lambda = 0.177 GeV
* istruc=35 -> cteq3m      (msbar), Lambda = 0.239 GeV
* istruc=36 -> mrsj        (msbar), Lambda = 0.366 GeV
* istruc=37 -> mrsjp       (msbar), Lambda = 0.542 GeV
* istruc=38 -> cteqjet     (msbar), Lambda = 0.286 GeV
* istruc=39 -> mrsr1       (msbar), Lambda = 0.241 GeV
* istruc=40 -> mrsr2       (msbar), Lambda = 0.344 GeV
* istruc=41 -> mrsr3       (msbar), Lambda = 0.241 GeV 
* istruc=42 -> mrsr4       (msbar), Lambda = 0.344 GeV 
* istruc=43 -> cteq4l      (msbar), Lambda = 0.300 GeV
* istruc=44 -> cteq4m      (msbar), Lambda = 0.300 GeV
* istruc=45 -> cteq4hj     (msbar), Lambda = 0.300 GeV
* istruc=46 -> cteq4lq     (msbar), Lambda = 0.300 GeV
*
*
* extra structure functions with variable as/Lambda
*
* istruc=100 -> mrsap       (msbar), as(Mz) = 0.105 -> Lambda = 0.150
* istruc=101 -> mrsap       (msbar), as(Mz) = 0.110 -> Lambda = 0.216
* istruc=102 -> mrsap       (msbar), as(Mz) = 0.115 -> Lambda = 0.284
* istruc=103 -> mrsap       (msbar), as(Mz) = 0.120 -> Lambda = 0.366
* istruc=104 -> mrsap       (msbar), as(Mz) = 0.125 -> Lambda = 0.458
* istruc=105 -> mrsap       (msbar), as(Mz) = 0.130 -> Lambda = 0.564
* istruc=111 -> grv94       (msbar), Lambda = 0.150 GeV
* istruc=112 -> grv94       (msbar), Lambda = 0.200 GeV = istruc33
* istruc=113 -> grv94       (msbar), Lambda = 0.250 GeV
* istruc=114 -> grv94       (msbar), Lambda = 0.300 GeV
* istruc=115 -> grv94       (msbar), Lambda = 0.350 GeV
* istruc=116 -> grv94       (msbar), Lambda = 0.400 GeV
* istruc=121 -> cteq3m      (msbar), Lambda = 0.100 GeV
* istruc=122 -> cteq3m      (msbar), Lambda = 0.120 GeV
* istruc=123 -> cteq3m      (msbar), Lambda = 0.140 GeV
* istruc=124 -> cteq3m      (msbar), Lambda = 0.180 GeV
* istruc=125 -> cteq3m      (msbar), Lambda = 0.200 GeV
* istruc=126 -> cteq3m      (msbar), Lambda = 0.220 GeV
* istruc=127 -> cteq3m      (msbar), Lambda = 0.260 GeV
* istruc=128 -> cteq3m      (msbar), Lambda = 0.300 GeV
* istruc=129 -> cteq3m      (msbar), Lambda = 0.340 GeV
* istruc=130 -> cteq4m      (msbar), Lambda = 0.215 GeV
* istruc=131 -> cteq4m      (msbar), Lambda = 0.255 GeV
* istruc=132 -> cteq4m      (msbar), Lambda = 0.300 GeV = istruc44
* istruc=133 -> cteq4m      (msbar), Lambda = 0.348 GeV
* istruc=134 -> cteq4m      (msbar), Lambda = 0.401 GeV
*
*
* And proton/antiproton flag (ipp=0 -> proton, ipp=1 -> anti proton)
*
      istruc=35
      ipp1=0
      ipp2=0
*
* renormalization & factorization scale :
*     Q_ren=crenorm * scale
*     Q_fac=cfact   * scale
*     scale^2  = 1. (total invariant mass)^2
*                2. vector boson mass (dynamical)
*                3. vector boson mass (on-shell )
*                4. M(V)^2 + P_t(V)^2
*                5. leading jet Et
*                6. P_T(V)
*

      irenorm = 1
      crenorm = 1.0d0 !1d-3*(10.0d0**i)
      ifact  =irenorm
      cfact  =crenorm
*
* physics parameters :
*
* center of mass energy
*
*
* w mass and width
*
      rmw=80.41d0
      rgw=2.047d0
*
* z mass and width
*
      rmz=91.188d0
      rgz=2.44d0

* w mass and width
*
      rmw=80.385d0
      rgw=2.093d0
*
* z mass and width
*
      rmz=91.187d0
      rgz=2.4952d0
*
* sin^2 weak angle
*
      sw2=1d0-(rmw/rmz)**2
      print*,'sw2=',sw2
*      sw2=0.2236d0
*
*
*  lambda qcd, determined by structure function choise
*
      if (istruc.eq.1) qcdl=0.1d0
      if (istruc.eq.2) qcdl=0.2d0
      if (istruc.eq.3) qcdl=0.19d0
      if (istruc.eq.4) qcdl=0.19d0
      if (istruc.eq.5) qcdl=0.19d0
      if (istruc.eq.6) qcdl=0.19d0
      if (istruc.eq.7) qcdl=0.19d0
      if (istruc.eq.8) qcdl=0.144d0
      if (istruc.eq.9) qcdl=0.155d0
      if (istruc.eq.10) qcdl=0.194d0
      if (istruc.eq.11) qcdl=0.191d0
      if (istruc.eq.12) qcdl=0.237d0
      if (istruc.eq.13) qcdl=0.215d0
      if (istruc.eq.14) qcdl=0.215d0
      if (istruc.eq.15) qcdl=0.215d0
      if (istruc.eq.16) qcdl=0.230d0
      if (istruc.eq.17) qcdl=0.230d0
      if (istruc.eq.18) qcdl=0.230d0
      if (istruc.eq.19) qcdl=0.168d0
      if (istruc.eq.20) qcdl=0.231d0
      if (istruc.eq.21) qcdl=0.322d0
      if (istruc.eq.22) qcdl=0.231d0
      if (istruc.eq.23) qcdl=0.200d0
      if (istruc.eq.24) qcdl=0.190d0
      if (istruc.eq.25) qcdl=0.213d0
      if (istruc.eq.26) qcdl=0.322d0
      if (istruc.eq.27) qcdl=0.208d0
      if (istruc.eq.28) qcdl=0.208d0
      if (istruc.eq.29) qcdl=0.230d0
      if (istruc.eq.30) qcdl=0.230d0
      if (istruc.eq.31) qcdl=0.255d0
      if (istruc.eq.32) qcdl=0.231d0
      if (istruc.eq.33) qcdl=0.200d0
      if (istruc.eq.34) qcdl=0.177d0
      if (istruc.eq.35) qcdl=0.239d0
      if (istruc.eq.36) qcdl=0.366d0
      if (istruc.eq.37) qcdl=0.542d0
      if (istruc.eq.38) qcdl=0.286d0
      if (istruc.eq.39) qcdl=0.241d0
      if (istruc.eq.40) qcdl=0.344d0
      if (istruc.eq.41) qcdl=0.241d0
      if (istruc.eq.42) qcdl=0.344d0
      if (istruc.eq.43) qcdl=0.300d0
      if (istruc.eq.44) qcdl=0.300d0
      if (istruc.eq.45) qcdl=0.300d0
      if (istruc.eq.46) qcdl=0.300d0
      if (istruc.eq.100) qcdl=0.160d0
      if (istruc.eq.101) qcdl=0.216d0
      if (istruc.eq.102) qcdl=0.284d0
      if (istruc.eq.103) qcdl=0.366d0
      if (istruc.eq.104) qcdl=0.458d0
      if (istruc.eq.105) qcdl=0.564d0
      if (istruc.eq.111) qcdl=0.150d0
      if (istruc.eq.112) qcdl=0.200d0
      if (istruc.eq.113) qcdl=0.250d0
      if (istruc.eq.114) qcdl=0.300d0
      if (istruc.eq.115) qcdl=0.350d0
      if (istruc.eq.116) qcdl=0.400d0
      if (istruc.eq.121) qcdl=0.100d0
      if (istruc.eq.122) qcdl=0.120d0
      if (istruc.eq.123) qcdl=0.140d0
      if (istruc.eq.124) qcdl=0.180d0
      if (istruc.eq.125) qcdl=0.200d0
      if (istruc.eq.126) qcdl=0.220d0
      if (istruc.eq.127) qcdl=0.260d0
      if (istruc.eq.128) qcdl=0.300d0
      if (istruc.eq.129) qcdl=0.340d0
      if (istruc.eq.130) qcdl=0.215d0
      if (istruc.eq.131) qcdl=0.255d0
      if (istruc.eq.132) qcdl=0.300d0
      if (istruc.eq.133) qcdl=0.348d0
      if (istruc.eq.134) qcdl=0.401d0
*
      
      call GetOrderAs(iorder)
* Clean up code.
      
*      iorder=nloop
*
* setup variables used in monte carlo
      call setup
*
* output settings of monte carlo
*
      write(*,*)
      write(*,*) '*************************************************'
      write(*,*) '                                  r 4.1 d 17/7/97'
      spp1=' '
      spp2=' '
      if (ipp1.eq.0) spp1='p'
      if (ipp1.eq.1) spp1='pbar'
      if (ipp2.eq.0) spp2='p'
      if (ipp2.eq.1) spp2='pbar'
      if (exclusive) then
        sex ='exclusive'
      else
        sex ='inclusive'
      endif
      if ((spp1.eq.' ').or.(spp2.eq.' ')) then
        write(*,*) '*** input error, ipp1, ipp2 =',ipp1,ipp2
	goto 999
      endif
      svec=' '
      if (ivec.eq.0) svec='W'
      if (ivec.eq.1) svec='W-'
      if (ivec.eq.2) svec='W+'
      if (ivec.eq.3) svec='Z'
      if (svec.eq.' ') then
        write(*,*) '*** input error, ivec =',ivec
        goto 999
      endif
      if (njets+nloop.gt.2) then
        write(*,*) '*** input error, njets+nloop =',njets+nloop
        goto 999
      endif
      write(*,11) spp1,spp2,svec,njets,sex,(njets+nloop),w
   11 format(/,1x,a4,a4,'--> ',a2,' + ',i2,' jets ',a9,
     .      ' at O(alphas**',i1,') at ',f8.0,' GeV ')
      write(*,12) delrjj,rsep,etminj,etmaxj,rapminj,rapmaxj
   12 format(/,' jet defining cuts ',/,/,
     .         ' delta Rjj = ',f4.2,',     Rsep = ',f4.2,/,/,
     .        '    ',f7.2,' GeV <   ETjet  < ',f7.2,' GeV ',/,/,
     .        '       ',f4.2,'     < |etajet| <    ',f4.2)
      sjet=' '
      if(jalg1.eq.1)sjet=' deltaR(i,j)   < delrjj  '
      if(jalg1.eq.2)sjet=' deltaR(i,jet) < delrjj  '
      if(jalg1.eq.3)sjet=' kt algorithm; R = delrjj'
      if(jalg1.eq.4)sjet=' deltaR(i,jet) < delrjj , deltaR(i,j) < rsep'
      write(*,13) sjet
   13 format(/,a47,' clustering')
      if(jalg2.eq.1)sjet=' D0 eta/phi'
      if(jalg2.eq.2)sjet=' Snowmass  '
      if(jalg2.eq.3)sjet=' 4 momentum'
      if(jalg2.eq.4)sjet=' 4 momentum'
      write(*,14) sjet
   14 format(/,a12,' recombination')
      if (ivec.eq.3) then
        write(*,15) etminl,delrjl,rapmaxl
      else
        write(*,16) etminl,etmis,delrjl,rapmaxl
      endif
   15 format(/,' lepton cuts [et,r,eta] : ['
     .       ,f5.2,',',f5.2,',',f5.2,']')
   16 format(/,' lepton cuts [et,etmis,r,eta] : ['
     .       ,f5.2,',',f5.2,',',f5.2,',',f5.2,']')
      write(*,17) raphad
   17 format(/,' hadron rapidity coverage : ',f5.2)
      if (ireson.eq.0) sreson='breit wigner'
      if (ireson.eq.1) sreson='narrow width'
      if (sreson.eq.' ') then
        write(*,*) '*** input error, ireson =',ireson
        goto 999
      endif
      sstru=' '
      if (istruc.eq.1)  sstru='mrse    (msbar)'
      if (istruc.eq.2)  sstru='mrsb    (msbar)'
      if (istruc.eq.3)  sstru='kmrshb  (msbar)'
      if (istruc.eq.4)  sstru='kmrsb0  (msbar)'
      if (istruc.eq.5)  sstru='kmrsb-  (msbar)'
      if (istruc.eq.6)  sstru='kmrsb-5 (msbar)'
      if (istruc.eq.7)  sstru='kmrsb-2 (msbar)'
      if (istruc.eq.8)  sstru='mts1    (msbar)'
      if (istruc.eq.9)  sstru='mte1    (msbar)'
      if (istruc.eq.10) sstru='mtb1    (msbar)'
      if (istruc.eq.11) sstru='mtb2    (msbar)'
      if (istruc.eq.12) sstru='mtsn1   (msbar)'
      if (istruc.eq.13) sstru='kmrss0  (msbar)'
      if (istruc.eq.14) sstru='kmrsd0  (msbar)'
      if (istruc.eq.15) sstru='kmrsd-  (msbar)'
      if (istruc.eq.16) sstru='mrss0   (msbar)'
      if (istruc.eq.17) sstru='mrsd0   (msbar)'
      if (istruc.eq.18) sstru='mrsd-   (msbar)'
      if (istruc.eq.19) sstru='cteq1l  (msbar)'
      if (istruc.eq.20) sstru='cteq1m  (msbar)'
      if (istruc.eq.21) sstru='cteq1ml (msbar)'
      if (istruc.eq.22) sstru='cteq1ms (msbar)'
      if (istruc.eq.23) sstru='grv     (msbar)'
      if (istruc.eq.24) sstru='cteq2l  (msbar)'
      if (istruc.eq.25) sstru='cteq2m  (msbar)'
      if (istruc.eq.26) sstru='cteq2ml (msbar)'
      if (istruc.eq.27) sstru='cteq2ms (msbar)'
      if (istruc.eq.28) sstru='cteq2mf (msbar)'
      if (istruc.eq.29) sstru='mrsh    (msbar)'
      if (istruc.eq.30) sstru='mrsa    (msbar)'
      if (istruc.eq.31) sstru='mrsg    (msbar)'
      if (istruc.eq.32) sstru='mrsap   (msbar)'
      if (istruc.eq.33) sstru='grv94   (msbar)'
      if (istruc.eq.34) sstru='cteq3l  (msbar)'
      if (istruc.eq.35) sstru='cteq3m  (msbar)'
      if (istruc.eq.36) sstru='mrsj    (msbar)'
      if (istruc.eq.37) sstru='mrsjp   (msbar)'
      if (istruc.eq.38) sstru='cteqjet (msbar)'
      if (istruc.eq.39) sstru='mrsr1   (msbar)'
      if (istruc.eq.40) sstru='mrsr2   (msbar)'
      if (istruc.eq.41) sstru='mrsr3   (msbar)'
      if (istruc.eq.42) sstru='mrsr4   (msbar)'
      if (istruc.eq.43) sstru='cteq4l  (msbar)'
      if (istruc.eq.44) sstru='cteq4m  (msbar)'
      if (istruc.eq.45) sstru='cteq4hj (msbar)'
      if (istruc.eq.46) sstru='cteq4lq (msbar)'
      if (istruc.eq.100) sstru='mrsap  (0.105)'
      if (istruc.eq.101) sstru='mrsap  (0.110)'
      if (istruc.eq.102) sstru='mrsap  (0.115)'
      if (istruc.eq.103) sstru='mrsap  (0.120)'
      if (istruc.eq.104) sstru='mrsap  (0.125)'
      if (istruc.eq.105) sstru='mrsap  (0.130)'
      if (istruc.eq.111) sstru='grv94  (msbar)'
      if (istruc.eq.112) sstru='grv94  (msbar)'
      if (istruc.eq.113) sstru='grv94  (msbar)'
      if (istruc.eq.114) sstru='grv94  (msbar)'
      if (istruc.eq.115) sstru='grv94  (msbar)'
      if (istruc.eq.116) sstru='grv94  (msbar)'
      if (istruc.eq.121) sstru='cteq3m (msbar)'
      if (istruc.eq.122) sstru='cteq3m (msbar)'
      if (istruc.eq.123) sstru='cteq3m (msbar)'
      if (istruc.eq.124) sstru='cteq3m (msbar)'
      if (istruc.eq.125) sstru='cteq3m (msbar)'
      if (istruc.eq.126) sstru='cteq3m (msbar)'
      if (istruc.eq.127) sstru='cteq3m (msbar)'
      if (istruc.eq.128) sstru='cteq3m (msbar)'
      if (istruc.eq.129) sstru='cteq3m (msbar)'
      if (istruc.eq.130) sstru='cteq4m (msbar)'
      if (istruc.eq.131) sstru='cteq4m (msbar)'
      if (istruc.eq.132) sstru='cteq4m (msbar)'
      if (istruc.eq.133) sstru='cteq4m (msbar)'
      if (istruc.eq.134) sstru='cteq4m (msbar)'
      sstru=namepdfset
      if (sstru.eq.' ') then
        write(*,*) '*** input error, istruc =',istruc
        goto 999
      endif
      write(*,18) sstru
   18 format(/,' structure function : ',a32)
      write(*,19) smin
   19 format(/,' parton resolution cut : ',g12.5)
      write(*,20) safety,s0 

   20 format(/,' safety cut = smin / ',f8.2,' = ',g12.5,' GeV^2')
      if (ivec.eq.3) then
        write(*,21) svec,rmz,rgz,sreson
      else
        write(*,21) svec,rmw,rgw,sreson
      endif
   21 format(/,1x,a1,' mass = ',f5.2,' and width = ',f5.2
     .     ,'  ( ',a13,')')
      sir=' '
      if (irenorm.eq.1) sir='total invariant mass'
      if (irenorm.eq.2) sir='vector boson mass (dynamical)'
      if (irenorm.eq.3) sir='vector boson mass (on-shell )'
      if (irenorm.eq.4) sir='sqrt(M(V)^2+P_T(V)^2) '
      if (irenorm.eq.5) sir='E_T of leading jet '
      if (irenorm.eq.6) sir='P_T(V) '
      if (sir.eq.' ') then
        write(*,*) '*** input error, irenorm =',irenorm
	goto 999
      endif
      write(*,22) crenorm,sir
   22 format(/,' renormalization scale is ',f5.2,' * ',a32)
      sir=' '
      if (ifact.eq.1) sir='total invariant mass'
      if (ifact.eq.2) sir='vector boson mass (dynamical)'
      if (ifact.eq.3) sir='vector boson mass (on-shell )'
      if (ifact.eq.4) sir='sqrt(M(V)^2+P_t(V)^2) '
      if (ifact.eq.5) sir='E_T of leading jet '
      if (ifact.eq.6) sir='P_T(V) '
      if (sir.eq.' ') then
        write(*,*) '*** input error, ifact =',ifact
	goto 999
      endif
      write(*,23) cfact,sir
   23 format(/,' factorization scale is   ',f5.2,' * ',a32)
      if(iorder.eq.0)then
        as=alphasPDF(rmz)
        sstru='one loop'
      elseif(iorder.eq.1)then
        as=alphasPDF(rmz)
        sstru=' two loop'
      endif
      write(*,24)nf,qcdl,sstru,as
   24 format(/,' active quark flavours              =   ',i6,/,/,
     .         ' QCD lambda for 4 flavours          =   ',f6.3,/,/,
     .         a9,' alphas(m_z)               = ',f8.5)
      write(*,25) itmax2,nshot2,nshot4 
   25 format(/,' number of events : ',i3,
     .         '*(',i8,' + ',i8,' )')

      write(*,26) sw2 
   26 format(/,' sin^2  = ',f5.2)
      write(*,*)
      write(*,*) '=================================================',
     . '=================' 
      write(*,*) 
*
* calculate cross section
*
      call cpu_time(starttime)
      call cross(sig,sd,chi)
      call cpu_time(finishtime)
*
* output results and distributions
*
      write(*,30) svec,njets,sig(0),sd(0),chi(0)
   30 format(/,'  sigma(',a2,' + ',i2,' jets) = ',g12.5,'  +/-',g12.5,
     . ' pb ',' (p=',g12.5,')'/)
      if (plot) call bino(4,0d0,0)

      write(*,*) 'xyz-1',smin,sig(0),sd(0),sig(1),sd(1),sig(2),sd(2),finishtime-starttime
      write(*,*) 'xyz-2',ymin,sig(0),sd(0),sig(1),sd(1),sig(2),sd(2),finishtime-starttime
*
  999 continue
      write(*,*)
      write(*,*) '*************************************************',
     . '*****************' 
      write(*,*)
*
 1000 continue

      enddo ! loop over ymin values
      end
*
************************************************************************ 
*
